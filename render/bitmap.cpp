//nya-engine (C) nyan.developer@gmail.com released under the MIT license (see LICENSE)

#include "bitmap.h"
#include "memory/tmp_buffer.h"
#include <string.h>
#include <vector>

namespace nya_render
{

void bitmap_downsample2x(unsigned char *data,int width,int height,int channels)
{
    bitmap_downsample2x(data,width,height,channels,data);
}

void bitmap_downsample2x(const unsigned char *data,int width,int height,int channels,unsigned char *out)
{
    for(unsigned int h=0;h<height/2;++h)
    {
        const unsigned char *hdata=data+(h*channels*2)*width;
        for(unsigned int w=0;w<width/2;++w,hdata+=channels*2)
        {
            const unsigned char *wdata=hdata;
            for(unsigned int c=0;c<channels;++c,++wdata)
            {
                *out++ =(wdata[0]+
                         wdata[channels]+
                         wdata[channels*width]+
                         wdata[channels*width+channels])/4;
            }
        }
    }
}

inline void swap(unsigned char *a,unsigned char *b,int channels)
{
    unsigned char tmp;
    for(int i=0;i<channels;++i)
        tmp=a[i],a[i]=b[i],b[i]=tmp;
}

void bitmap_flip_vertical(unsigned char *data,int width,int height,int channels)
{
    const int line_size=width*channels;
    const int half=line_size*(height/2);
    const int top=line_size*(height-1);
    std::vector<unsigned char> line_data(line_size);
    unsigned char *line=&line_data[0], *from=data, *to=data+top;

    for(int offset=0;offset<half;offset+=line_size)
    {
        unsigned char *f=from+offset, *t=to-offset;
        memcpy(line,f,line_size);
        memcpy(f,t,line_size);
        memcpy(t,line,line_size);
    }
}

void bitmap_flip_vertical(const unsigned char *data,int width,int height,int channels,unsigned char *out)
{
    const int line_size=width*channels;
    out+=line_size*(height-1);
    for(int y=0;y<height;++y,data+=line_size,out-=line_size)
        memcpy(out,data,line_size);
}

void bitmap_flip_horisontal(unsigned char *data,int width,int height,int channels)
{
    const int line_size=width*channels;
    const int size=width*height*channels;
    const int half=line_size/2;

    for(size_t offset=0;offset<size;offset+=line_size)
    {
        unsigned char *ha=data+offset;
        unsigned char *hb=ha+line_size-channels;

        for(int w=0;w<half;w+=channels)
            swap(ha+w,hb-w,channels);
    }
}

void bitmap_flip_horisontal(const unsigned char *data,int width,int height,int channels,unsigned char *out)
{
    const int line_size=width*channels;
    out+=(width-1)*channels;
    for(int y=0;y<height;++y,data+=line_size,out+=line_size)
    {
        for(int w=0;w<line_size;w+=channels)
            memcpy(out-w,data+w,channels);
    }
}

void bitmap_rotate_90_left(unsigned char *data,int width,int height,int channels)
{
    if(width!=height)
    {
        nya_memory::tmp_buffer_scoped buf(width*height*channels);
        bitmap_rotate_90_left(data,width,height,channels,(unsigned char *)buf.get_data());
        buf.copy_to(data,buf.get_size());
        return;
    }

    for(int y=0;y<width;++y)
    {
        for(int x=y+1;x<width;++x)
            swap(data+(y*width+x)*channels,data+(x*width+y)*channels,channels);
    }

    bitmap_flip_horisontal(data,width,height,channels);
}

void bitmap_rotate_90_left(const unsigned char *data,int width,int height,int channels,unsigned char *out)
{
    for(int x=0;x<height;++x)
    {
        for(int y=0;y<width;++y,data+=channels)
            memcpy(out+(y*height+height-x-1)*channels,data,channels);
    }
}

void bitmap_rotate_90_right(unsigned char *data,int width,int height,int channels)
{
    if(width!=height)
    {
        nya_memory::tmp_buffer_scoped buf(width*height*channels);
        bitmap_rotate_90_right(data,width,height,channels,(unsigned char *)buf.get_data());
        buf.copy_to(data,buf.get_size());
        return;
    }

    for(int y=0;y<width;++y)
    {
        for(int x=y+1;x<width;++x)
            swap(data+(y*width+x)*channels,data+(x*width+y)*channels,channels);
    }

    bitmap_flip_vertical(data,width,height,channels);
}

void bitmap_rotate_90_right(const unsigned char *data,int width,int height,int channels,unsigned char *out)
{
    for(int x=0;x<height;++x)
    {
        for(int y=0;y<width;++y,data+=channels)
            memcpy(out+((width-y-1)*height+x)*channels,data,channels);
    }
}

void bitmap_rotate_180(unsigned char *data,int width,int height,int channels)
{
    const int line_size=width*channels;
    const int size=width*height*channels;
    const int top=line_size*(height-1);
    const int half=line_size/2;

    for(size_t offset=0;offset<size;offset+=line_size)
    {
        unsigned char *ha=data+top-offset;
        unsigned char *hb=data+offset+line_size-channels;

        for(int w=0;w<half;w+=channels)
            swap(ha+w,hb-w,channels);
    }

    if((width%2)==1)
    {
        for(int y=0;y<height/2;++y)
            swap(data+(width*y+width/2)*channels,data+(width*(height-y-1)+width/2)*channels,channels);
    }
}

void bitmap_rotate_180(const unsigned char *data,int width,int height,int channels,unsigned char *out)
{
    out+=(height-1)*width*channels+(width-1)*channels;
    for(int i=0;i<width*height;++i,out-=channels,data+=channels)
        memcpy(out,data,channels);
}

void bitmap_rgb_to_bgr(unsigned char *data,int width,int height,int channels)
{
    for(int i=0;i<width*height;++i,data+=channels)
    {
        const unsigned char tmp=data[0];
        data[0]=data[2];
        data[2]=tmp;
    }
}

void bitmap_rgb_to_bgr(const unsigned char *data,int width,int height,int channels,unsigned char *out)
{
    if(channels==3)
    {
        for(int i=0;i<width*height;++i,data+=3,out+=3)
            out[0]=data[2],out[1]=data[1],out[2]=data[0];
    }
    else if(channels==4)
    {
        for(int i=0;i<width*height;++i,data+=4,out+=4)
            out[0]=data[2],out[1]=data[1],out[2]=data[0],out[3]=data[3];
    }
}

void bitmap_rgba_to_rgb(unsigned char *data,int width,int height)
{
    const unsigned char *from=data;
    for(int i=1;i<width*height;++i)
        memcpy(data+=3,from+=4,3);
}

void bitmap_rgba_to_rgb(const unsigned char *data,int width,int height,unsigned char *out)
{
    for(int i=0;i<width*height;++i,data+=4,out+=3)
        memcpy(out,data,3);
}

void bitmap_rgb_to_rgba(const unsigned char *data,int width,int height,unsigned char alpha,unsigned char *out)
{
    for(int i=0;i<width*height;++i)
    {
        memcpy(out,data,3);
        out+=3,data+=3;
        *out++=alpha;
    }
}

inline void color_to_yuv420(const unsigned char *data[3],int width,int height,int channels,unsigned char *out)
{
    const size_t image_size=width*height;
    unsigned char *dst_y=out;
    unsigned char *dst_u=dst_y+image_size;
    unsigned char *dst_v=dst_u+image_size/4;

    for(size_t i=0;i<image_size*channels;i+=channels)
        *dst_y++ =((66*data[0][i] + 129*data[1][i] + 25*data[2][i])>>8)+16;

    for(int y=0;y<height;y+=2)
    {
        for(int x=0;x<width;x+=2)
        {
            const size_t i=(y*width + x)*channels;
            *dst_u++ =((-38*data[0][i] - 74*data[1][i] + 112*data[2][i])>>8)+128;
            *dst_v++ =((112*data[0][i] - 94*data[1][i] -  18*data[2][i])>>8)+128;
        }
    }
}

void bitmap_rgb_to_yuv420(const unsigned char *data,int width,int height,int channels,unsigned char *out)
{
    const unsigned char *rgb[]={data,data+1,data+2};
    color_to_yuv420(rgb,width,height,channels,out);
}

void bitmap_bgr_to_yuv420(const unsigned char *data,int width,int height,int channels,unsigned char *out)
{
    const unsigned char *bgr[]={data+2,data+1,data};
    color_to_yuv420(bgr,width,height,channels,out);
}

inline unsigned char clamp(int c) { return c<0?0:(c>255?255:c); }

void bitmap_yuv420_to_rgb(const unsigned char *data,int width,int height,unsigned char *out)
{
    const size_t image_size=width*height;
    const unsigned char *udata=data+image_size;
    const unsigned char *vdata=udata+image_size/4;
    const int half_width=width/2;

    for(int y=0;y<height;++y)
    {
        const int hidx=y/2*half_width;
        for(int x=0;x<width;++x)
        {
            const int y0=(int)*data++;
            const int idx=hidx+x/2;
            const int u0=(int)udata[idx]-128;
            const int v0=(int)vdata[idx]-128;

            const int y1=1192*(y0>16?y0-16:0);
            const int u1=400*u0, u2=2066*u0;
            const int v1=1634*v0, v2=832*v0;

            *out++ =clamp((y1+v1)>>10);
            *out++ =clamp((y1-v2-u1)>>10);
            *out++ =clamp((y1+u2)>>10);
        }
    }
}

}

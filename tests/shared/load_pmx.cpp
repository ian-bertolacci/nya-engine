//nya-engine (C) nyan.developer@gmail.com released under the MIT license (see LICENSE)

#include "load_pmx.h"
#include "scene/mesh.h"
#include "memory/memory_reader.h"
#include "string_encoding.h"

#include "resources/resources.h"

namespace
{

struct add_data: public pmx_loader::additional_data, nya_scene::shared_mesh::additional_data
{
    const char *type() { return "pmx"; }
};

int read_idx(nya_memory::memory_reader &reader,int size)
{
    switch(size)
    {
        case 1: return reader.read<char>();
        case 2: return reader.read<short>();
        case 4: return reader.read<int>();
    }

    return 0;
}

inline std::string read_string(nya_memory::memory_reader &reader,char encoding)
{
    const int name_len=reader.read<int>();
    std::string str=encoding?std::string((const char*)reader.get_data(),name_len):
    utf8_from_utf16le(reader.get_data(),name_len);
    reader.skip(name_len);
    return str;
}

inline void skip_string(nya_memory::memory_reader &reader) { reader.skip(reader.read<int>()); }

template<typename t> void load_vertex_morph(nya_memory::memory_reader &reader,pmx_loader::additional_data::vert_morph &m)
{
    for(size_t j=0;j<m.verts.size();++j)
    {
        pmd_morph_data::morph_vertex &v=m.verts[j];
        v.idx=reader.read<t>();
        v.pos.x=reader.read<float>();
        v.pos.y=reader.read<float>();
        v.pos.z=-reader.read<float>();
    }
}

template<typename t> void load_uv_morph(nya_memory::memory_reader &reader,pmx_loader::additional_data::uv_morph &m)
{
    for(size_t j=0;j<m.verts.size();++j)
    {
        add_data::morph_uv &v=m.verts[j];
        v.idx=reader.read<t>();
        v.tc.x=reader.read<float>();
        v.tc.y=-reader.read<float>();
        reader.skip(2*4); //ToDo
    }
}

inline void read_vector(nya_math::vec3 &out,nya_memory::memory_reader &reader)
{
    out.x=reader.read<float>(),out.y=reader.read<float>(),out.z=-reader.read<float>();
}

inline void read_angle(nya_math::vec3 &out,nya_memory::memory_reader &reader)
{
    out.x=-reader.read<float>(),out.y=-reader.read<float>(),out.z=reader.read<float>();
}

}

bool pmx_loader::load(nya_scene::shared_mesh &res,nya_scene::resource_data &data,const char* name)
{
    if(!data.get_size())
        return false;

    nya_memory::memory_reader reader(data.get_data(),data.get_size());
    if(!reader.test("PMX",3))
        return false;

    reader.skip(1);
    if(reader.read<float>()!=2.0f)
    {
        nya_log::log()<<"pmx load error: invalid version\n";
        return false;
    }

    const char header_size=reader.read<char>();
    if(header_size!=sizeof(pmx_header))
    {
        nya_log::log()<<"pmx load error: invalid header\n";
        return false;
    }
    
    const pmx_header header=reader.read<pmx_header>();
    
    for(int i=0;i<4;++i)
    {
        const int size=reader.read<int>();
        reader.skip(size);
    }
    
    const int vert_count=reader.read<int>();
    if(!vert_count)
    {
        nya_log::log()<<"pmx load error: no verts found\n";
        return false;
    }

    std::vector<vert> verts(vert_count);

    for(int i=0;i<vert_count;++i)
    {
        vert &v=verts[i];
        read_vector(v.pos,reader);
        read_vector(v.normal,reader);

        v.tc.x=reader.read<float>();
        v.tc.y=1.0f-reader.read<float>();
        reader.skip(header.extended_uv*sizeof(float)*4);

        switch(reader.read<char>())
        {
            case 0:
                v.bone_idx[0]=(float)read_idx(reader,header.bone_idx_size);
                v.bone_weight[0]=1.0f;
                for(int j=1;j<4;++j)
                    v.bone_weight[j]=v.bone_idx[j]=0.0f;
                break;
                
            case 1:
                v.bone_idx[0]=(float)read_idx(reader,header.bone_idx_size);
                v.bone_idx[1]=(float)read_idx(reader,header.bone_idx_size);
                
                v.bone_weight[0]=reader.read<float>();
                v.bone_weight[1]=1.0f-v.bone_weight[0];
                
                for(int j=2;j<4;++j)
                    v.bone_weight[j]=v.bone_idx[j]=0.0f;
                break;
                
            case 2:
                for(int j=0;j<4;++j)
                    v.bone_idx[j]=read_idx(reader,header.bone_idx_size);
                
                for(int j=0;j<4;++j)
                    v.bone_weight[j]=reader.read<float>();
                break;
                
            case 3:
                v.bone_idx[0]=(float)read_idx(reader,header.bone_idx_size);
                v.bone_idx[1]=(float)read_idx(reader,header.bone_idx_size);
                
                v.bone_weight[0]=reader.read<float>();
                v.bone_weight[1]=1.0f-v.bone_weight[0];
                
                for(int j=2;j<4;++j)
                    v.bone_weight[j]=v.bone_idx[j]=0.0f;

                reader.skip(sizeof(float)*3*3);
                break;

            default:
                nya_log::log()<<"pmx load error: invalid skining\n";
                return false;
        }

        reader.read<float>(); //edge
    }

    const int indices_count=reader.read<int>();
    if(header.index_size==2)
        res.vbo.set_index_data(reader.get_data(),nya_render::vbo::index2b,indices_count);
    else if(header.index_size==4)
        res.vbo.set_index_data(reader.get_data(),nya_render::vbo::index4b,indices_count);
    else
    {
        nya_log::log()<<"pmx load error: invalid index size\n";
        return false;
    }

    reader.skip(indices_count*header.index_size);

    const int textures_count=reader.read<int>();
    std::vector<std::string> tex_names(textures_count);
    for(int i=0;i<textures_count;++i)
        tex_names[i]=read_string(reader,header.text_encoding);

    const int mat_count=reader.read<int>();
    res.groups.resize(mat_count);
    res.materials.resize(mat_count);
    
    std::string path(name);
    size_t p=path.rfind("/");
    if(p==std::string::npos)
        p=path.rfind("\\");
    if(p==std::string::npos)
        path.clear();
    else
        path.resize(p+1);

    //nya_resources::file_resources_provider frp; frp.set_folder(path.c_str()); for(nya_resources::resource_info *fri=frp.first_res_info();fri;fri=fri->get_next()) printf("%s\n",fri->get_name());

    const nya_math::vec3 mmd_light_dir=-nya_math::vec3(-0.5,-1.0,-0.5).normalize();
    //const nya_math::vec3 mmd_light_color=nya_math::vec3(154,154,154)/255.0;

    add_data *ad=new add_data;
    res.add_data=ad;

    ad->materials.resize(mat_count);

    for(int i=0,offset=0;i<mat_count;++i)
    {
        nya_scene::shared_mesh::group &g=res.groups[i];
        nya_scene::material &m = res.materials[i];

        for(int j=0;j<2;++j)
        {
            const int name_len=reader.read<int>();
            if(j==1)
            {
                std::string name((const char*)reader.get_data(),name_len);
                m.set_name(name.c_str());
            }
            reader.skip(name_len);
        }

        const pmx_material_params &params=ad->materials[i].params=reader.read<pmx_material_params>();

        const char unsigned flag=reader.read<unsigned char>();
        const pmx_edge_params &edge=ad->materials[i].edge_params=reader.read<pmx_edge_params>();
        ad->materials[i].edge_group_idx=-1;

        const int tex_idx=read_idx(reader,header.texture_idx_size);
        const int sph_tex_idx=read_idx(reader,header.texture_idx_size);
        const int sph_mode=reader.read<char>();
        const char toon_flag=reader.read<char>();
        
        int toon_tex_idx=-1;
        if(toon_flag==0)
        {
            toon_tex_idx=read_idx(reader,header.texture_idx_size);
            if(toon_tex_idx>=(int)tex_names.size())
            {
                nya_log::log()<<"pmx load error: invalid toon tex idx\n";
                return false;
            }
        }
        else if(toon_flag==1)
            toon_tex_idx=reader.read<char>();
        else
        {
            nya_log::log()<<"pmx load error: invalid toon flag\n";
            return false;
        }

        if(tex_idx>=(int)tex_names.size())
        {
            nya_log::log()<<"pmx load error: invalid tex idx\n";
            return false;
        }

        if(sph_tex_idx>=(int)tex_names.size())
        {
            nya_log::log()<<"pmx load error: invalid sph tex idx\n";
            return false;
        }

        const int comment_len=reader.read<int>();
        reader.skip(comment_len);

        g.name="mesh";
        g.offset=offset;
        g.count=reader.read<int>();
        g.material_idx=i;
        offset+=g.count;

        {
            nya_scene::texture tex;
            if(tex_idx<0 || !tex.load((path+tex_names[tex_idx]).c_str()))
                tex.build("\xff\xff\xff\xff",1,1,nya_render::texture::color_bgra);
            m.set_texture("diffuse",tex);
        }

        {
            bool loaded=false;
            nya_scene::texture tex;
            if(toon_flag>0)
            {
                char buf[255];
                sprintf(buf,"toon%02d.bmp",toon_tex_idx+1);
                if(nya_resources::get_resources_provider().has((path+buf).c_str()))
                    loaded=tex.load((path+buf).c_str());
                else
                    loaded=tex.load(buf);
            }
            else if(toon_tex_idx>=0)
                loaded=tex.load((path+tex_names[toon_tex_idx]).c_str());

            if(!loaded)
                tex.build("\xff\xff\xff\xff",1,1,nya_render::texture::color_bgra);

            nya_render::texture rtex=tex.internal().get_shared_data()->tex;
            rtex.set_wrap(nya_render::texture::wrap_clamp, nya_render::texture::wrap_clamp);
            m.set_texture("toon",tex);
        }

        {
            bool add=false,mult=false;
            nya_scene::texture tex;
            if(sph_tex_idx>=0 && tex.load((path+tex_names[sph_tex_idx]).c_str()))
            {
                if(sph_mode==2)
                    m.set_texture("env add",tex),add=true;
                else if(sph_mode==1)
                    m.set_texture("env mult",tex),mult=true;
            }

            if(!add)
            {
                nya_scene::texture tex;
                tex.build("\x00\x00\x00\x00",1,1,nya_render::texture::color_bgra);
                m.set_texture("env add",tex);
            }

            if(!mult)
            {
                nya_scene::texture tex;
                tex.build("\xff\xff\xff\xff",1,1,nya_render::texture::color_bgra);
                m.set_texture("env mult",tex);
            }
        }

        nya_scene::material::pass &p=m.get_pass(m.add_pass(nya_scene::material::default_pass));
        nya_scene::shader sh;
        sh.load("mmd.nsh");
        p.set_shader(sh);
        p.get_state().set_blend(true,nya_render::blend::src_alpha,nya_render::blend::inv_src_alpha);
        p.get_state().set_cull_face(!(flag & (1<<0)),nya_render::cull_face::cw);

        m.set_param(m.get_param_idx("light dir"),mmd_light_dir);
        m.set_param(m.get_param_idx("amb k"),params.ambient,1.0f);
        m.set_param(m.get_param_idx("diff k"),params.diffuse);
        m.set_param(m.get_param_idx("spec k"),params.specular,params.shininess);

        if(!(flag & (1<<4)))
            continue;

        ad->materials[i].edge_group_idx=(int)res.groups.size();

        res.groups.resize(res.groups.size()+1);
        nya_scene::shared_mesh::group &ge=res.groups.back();
        ge=res.groups[i];
        ge.name="edge";
        ge.material_idx=int(res.materials.size());
        res.materials.resize(res.materials.size()+1);
        nya_scene::material &me=res.materials.back();

        nya_scene::material::pass &pe=me.get_pass(me.add_pass(nya_scene::material::default_pass));
        nya_scene::shader she;
        she.load("mmd_edge.nsh");
        pe.set_shader(she);
        pe.get_state().set_blend(true,nya_render::blend::src_alpha,nya_render::blend::inv_src_alpha);
        pe.get_state().set_cull_face(true,nya_render::cull_face::ccw);
        me.set_param(me.get_param_idx("edge offset"),edge.width*0.02f,edge.width*0.02f,edge.width*0.02f,0.0f);
        me.set_param(me.get_param_idx("edge color"),edge.color);
    }

    typedef unsigned short ushort;

    const int bones_count=reader.read<int>();

    std::vector<pmx_bone> bones(bones_count);
    for(int i=0;i<bones_count;++i)
    {
        pmx_bone &b=bones[i];

        b.name=read_string(reader,header.text_encoding);
        skip_string(reader); //en name

        read_vector(b.pos,reader);
        b.idx=i;

        b.parent=read_idx(reader,header.bone_idx_size);
        b.order=reader.read<int>();

        const flag<ushort> f(reader.read<ushort>()); //ToDo
        if(f.c(0x0001))
            read_idx(reader,header.bone_idx_size);
        else
            reader.skip(sizeof(float)*3);

        b.bound.has_rot=f.c(0x0100);
        b.bound.has_pos=f.c(0x0200);
        if(b.bound.has_rot || b.bound.has_pos)
        {
            b.bound.src_idx=read_idx(reader,header.bone_idx_size);
            b.bound.k=reader.read<float>();
        }

        if(f.c(0x0400))
            reader.skip(sizeof(float)*3); //ToDo

        if(f.c(0x0800))
            reader.skip(sizeof(float)*3*2); //ToDo

        if(f.c(0x2000))
            reader.read<int>(); //ToDo

        b.ik.has=f.c(0x0020);
        if(b.ik.has)
        {
            b.ik.eff_idx=read_idx(reader,header.bone_idx_size);
            b.ik.count=reader.read<int>();
            b.ik.k=reader.read<float>();

            const int link_count=reader.read<int>();
            b.ik.links.resize(link_count);
            for(int j=0;j<link_count;++j)
            {
                pmx_bone::ik_link &l=b.ik.links[j];
                l.idx=read_idx(reader,header.bone_idx_size);
                l.has_limits=reader.read<char>()>0;
                if(l.has_limits)
                {
                    read_angle(l.from,reader);
                    read_angle(l.to,reader);
                    std::swap(l.from.xy(),l.to.xy());
                }
            }
        }
    }

    for(int i=0;i<int(bones.size());++i)
    {
        bool had_sorted=false;
        for(int j=0;j<int(bones.size());++j)
        {
            const int p=bones[j].parent;
            if(p<=j)
                continue;

            had_sorted=true;
            std::swap(bones[j],bones[p]);
            for(int k=0;k<(int)bones.size();++k)
            {
                if(bones[k].parent==j)
                    bones[k].parent=p;
                else if(bones[k].parent==p)
                    bones[k].parent=j;
            }
        }

        if(!had_sorted)
            break;
    }

    std::vector<int> old_bones(bones_count);
    for(int i=0;i<bones_count;++i)
        old_bones[bones[i].idx]=i;

    for(int i=0;i<bones_count;++i)
    {
        const pmx_bone &b=bones[i];

        if((b.bound.has_pos || b.bound.has_rot) && b.bound.src_idx>=0 && b.bound.src_idx<bones_count)
            res.skeleton.add_bound(old_bones[b.bound.src_idx],i,b.bound.k,b.bound.has_pos,b.bound.has_rot,true);

        if(res.skeleton.add_bone(b.name.c_str(),b.pos,nya_math::quat(),b.parent,true)!=i)
        {
            nya_log::log()<<"pmx load error: invalid bone\n";
            return false;
        }

        if(b.ik.has)
        {
            const int ik=res.skeleton.add_ik(i,old_bones[b.ik.eff_idx],b.ik.count,b.ik.k,true);
            for(int j=0;j<int(b.ik.links.size());++j)
            {
                const pmx_bone::ik_link &l=b.ik.links[j];
                if(l.has_limits)
                    res.skeleton.add_ik_link(ik,old_bones[l.idx],l.from,l.to,true);
                else
                    res.skeleton.add_ik_link(ik,old_bones[l.idx],true);
            }
        }
    }

    const int morphs_count=reader.read<int>();
    ad->morphs.resize(morphs_count);

    for(int i=0;i<morphs_count;++i)
    {
        additional_data::morph &m=ad->morphs[i];

        m.name=read_string(reader,header.text_encoding);
        skip_string(reader); //en name

        m.kind=pmd_morph_data::morph_kind(reader.read<char>());

        m.type=morph_type(reader.read<char>());
        switch(m.type)
        {
            case morph_type_group:
            {
                m.idx=(int)ad->group_morphs.size();
                ad->group_morphs.resize(ad->group_morphs.size()+1);
                additional_data::group_morph &m=ad->group_morphs.back();

                const int size=reader.read<int>();
                m.morphs.resize(size);
                for(int j=0;j<size;++j)
                {
                    m.morphs[j].first=read_idx(reader,header.morph_idx_size);
                    m.morphs[j].second=reader.read<float>();
                }
            }
            break;

            case morph_type_vertex:
            {
                m.idx=(int)ad->vert_morphs.size();
                ad->vert_morphs.resize(ad->vert_morphs.size()+1);
                additional_data::vert_morph &m=ad->vert_morphs.back();

                const int size=reader.read<int>();
                m.verts.resize(size);

                switch(header.index_size)
                {
                    case 1: load_vertex_morph<unsigned char>(reader,m); break;
                    case 2: load_vertex_morph<unsigned short>(reader,m); break;
                    case 4: load_vertex_morph<unsigned int>(reader,m); break;
                }
            }
            break;

            case morph_type_bone:
            {
                m.idx=(int)ad->bone_morphs.size();
                ad->bone_morphs.resize(ad->bone_morphs.size()+1);
                additional_data::bone_morph &m=ad->bone_morphs.back();

                const int size=reader.read<int>();
                m.bones.resize(size);

                for(int j=0;j<size;++j)
                {
                    additional_data::morph_bone &b=m.bones[j];
                    b.idx=read_idx(reader,header.bone_idx_size);
                    read_vector(b.pos,reader);

                    b.rot.v.x=-reader.read<float>();
                    b.rot.v.y=-reader.read<float>();
                    b.rot.v.z=reader.read<float>();
                    b.rot.w=reader.read<float>();
                }
            }
            break;

            case morph_type_uv:
            case morph_type_add_uv1:
            case morph_type_add_uv2:
            case morph_type_add_uv3:
            case morph_type_add_uv4:
            {
                m.idx=(int)ad->uv_morphs.size();
                ad->uv_morphs.resize(ad->uv_morphs.size()+1);
                additional_data::uv_morph &m=ad->uv_morphs.back();

                const int size=reader.read<int>();
                m.verts.resize(size);

                switch(header.index_size)
                {
                    case 1: load_uv_morph<unsigned char>(reader,m); break;
                    case 2: load_uv_morph<unsigned short>(reader,m); break;
                    case 4: load_uv_morph<unsigned int>(reader,m); break;
                }
            }
            break;

            case morph_type_material:
            {
                m.idx=(int)ad->mat_morphs.size();
                ad->mat_morphs.resize(ad->mat_morphs.size()+1);
                additional_data::mat_morph &m=ad->mat_morphs.back();

                const int size=reader.read<int>();
                m.mats.resize(size);

                for(int j=0;j<size;++j)
                {
                    additional_data::morph_mat &mm=m.mats[j];
                    mm.mat_idx=read_idx(reader,header.material_idx_size);
                    mm.op=additional_data::morph_mat::op_type(reader.read<char>());
                    mm.params=reader.read<pmx_material_params>();
                    mm.edge_params=reader.read<pmx_edge_params>();
                    mm.tex_color=reader.read<nya_math::vec4>();
                    mm.sp_tex_color=reader.read<nya_math::vec4>();
                    mm.toon_tex_color=reader.read<nya_math::vec4>();
                }
            }
            break;

            default:
                nya_log::log()<<"pmx load error: invalid morph type\n";
                return false;
        }
    }

    const int frames_count=reader.read<int>();
    for(int i=0;i<frames_count;++i)
    {
        skip_string(reader); //name
        skip_string(reader); //en name

        reader.skip(1);//flag

        const int inner_count=reader.read<int>();
        for(int j=0;j<inner_count;++j)
        {
            char type=reader.read<char>();
            if(type)
                read_idx(reader,header.morph_idx_size);
            else
                read_idx(reader,header.bone_idx_size);
        }
    }

    const int rigid_bodies_count=reader.read<int>();
    ad->rigid_bodies.resize(rigid_bodies_count);
    for(int i=0;i<rigid_bodies_count;++i)
    {
        pmd_phys_data::rigid_body &rb=ad->rigid_bodies[i];

        rb.name=read_string(reader,header.text_encoding);
        skip_string(reader); //en name

        rb.bone=read_idx(reader,header.bone_idx_size);
        if(rb.bone>=0)
            rb.bone=old_bones[rb.bone];

        rb.collision_group=reader.read<unsigned char>();
        rb.collision_mask=reader.read<ushort>();
        rb.type=(pmd_phys_data::shape_type)reader.read<unsigned char>();
        rb.size=reader.read<nya_math::vec3>();
        read_vector(rb.pos,reader);
        rb.pos-=res.skeleton.get_bone_original_pos(rb.bone);
        read_angle(rb.rot,reader);
        rb.mass=reader.read<float>();
        rb.vel_attenuation=reader.read<float>();
        rb.rot_attenuation=reader.read<float>();
        rb.restriction=reader.read<float>();
        rb.friction=reader.read<float>();
        rb.mode=(pmd_phys_data::object_type)reader.read<unsigned char>();
    }

    const int joints_count=reader.read<int>();
    ad->joints.resize(joints_count);
    for(int i=0;i<joints_count;++i)
    {
        pmd_phys_data::joint &j=ad->joints[i];
        j.name=read_string(reader,header.text_encoding);
        skip_string(reader); //en name

        char type=reader.read<char>();
        if(type>0)
        {
            nya_log::log()<<"pmx load warning: unsupported phys joint type\n";
            ad->joints.clear();
            ad->rigid_bodies.clear();
            break;
        }

        j.rigid_src=read_idx(reader,header.rigidbody_idx_size);
        j.rigid_dst=read_idx(reader,header.rigidbody_idx_size);
        read_vector(j.pos,reader);
        read_angle(j.rot,reader);

        read_vector(j.pos_min,reader);
        read_vector(j.pos_max,reader);
        std::swap(j.pos_max.z,j.pos_min.z);

        read_angle(j.rot_min,reader);
        read_angle(j.rot_max,reader);
        std::swap(j.rot_max.xy(),j.rot_min.xy());

        j.pos_spring=reader.read<nya_math::vec3>();
        read_angle(j.rot_spring,reader);
    }

    res.vbo.set_vertex_data(&verts[0],sizeof(vert),vert_count);
    int offset=0;
    res.vbo.set_vertices(offset,3); offset+=sizeof(verts[0].pos);
    res.vbo.set_normals(offset); offset+=sizeof(verts[0].normal);
    res.vbo.set_tc(0,offset,2); offset+=sizeof(verts[0].tc);
    res.vbo.set_tc(1,offset,4); offset+=sizeof(verts[0].bone_idx);
    res.vbo.set_tc(2,offset,4); //offset+=sizeof(verts[0].bone_weight);

    return true;
}

const pmx_loader::additional_data *pmx_loader::get_additional_data(const nya_scene::mesh &m)
{
    if(!m.internal().get_shared_data().is_valid())
        return 0;

    nya_scene::shared_mesh::additional_data *d=m.internal().get_shared_data()->add_data;
    if(!d)
        return 0;

    const char *type=d->type();
    if(!type || strcmp(type,"pmx")!=0)
        return 0;

    return static_cast<pmx_loader::additional_data*>(static_cast<add_data*>(d));
}
